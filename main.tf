locals {
  owner = "ncsu.ee2017@gmail.com"
  stack = "terraform-lambda-iam"
  name = "terraform-lambda-iam"
}

# terraform modules
module "lambda_iam" {
  source = "./iam/"
}


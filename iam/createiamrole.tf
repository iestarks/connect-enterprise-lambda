resource "aws_iam_group" "iam-group" {
  name = "iam-group"
}

resource "aws_iam_user" "iam-user" {
  name = "iam-user"
}


resource "aws_iam_group_membership" "iam-grp-membership" {
  name = "tf-iam-lambda-grp-membership"

  users = [
    aws_iam_user.iam-user.name,
  ]

  group = aws_iam_group.iam-group.name
}

resource "aws_iam_role" "lambda-function-role" {
  name = "lambda-tf-role"
  #assume_role_policy = file("assumerolepolicy.json")

assume_role_policy = jsonencode({

    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "Service": [
              "ec2.amazonaws.com",
              "lambda.amazonaws.com",
              "s3.amazonaws.com"
            
        ]
    },
        "Effect": "Allow",
        "Sid": ""
      },
    ]
})
    tags = {
    Name = "lambda_funct_role"
  }

}

// resource "aws_iam_role" "lambda-backup-role" {
//   name = "lambda-backup-role"
//   #assume_role_policy = file("lambdabackup-assumerolepolicy.json")
//     assume_role_policy = aws_iam_policy.lambda_policies.policy
//     tags = {
//     Name = "lambda_backup_role"
//   }

// }


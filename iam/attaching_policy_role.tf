
# Define policy ARNs as list
// variable "iam_policy_arn" {
//    description = "lambda terraform policy"
//   type = list
//   #default = [ "policy_arn='arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]
// }

resource "aws_iam_policy" "lambda_policies" {
  name = "lambda_role_polices"
  path = "/"
  description = "IAM policy for lambda functions"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Resource": "*",
            "Action": [
                "ec2:DescribeInstances",
                "ec2:CreateNetworkInterface",
                "ec2:AttachNetworkInterface",
                "ec2:DescribeNetworkInterfaces",
                "autoscaling:CompleteLifecycleAction"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [ "s3:*"
        ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action":  "logs:*",
            "Resource": "arn:aws:logs:*:*:*"
        },
        {
            "Effect": "Allow",
            "Action": "lambda:InvokeFunction",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "elasticloadbalancing:Describe*",
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "cloudwatch:ListMetrics",
                "cloudwatch:GetMetricStatistics",
                "cloudwatch:Describe*"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "autoscaling:Describe*",
            "Resource": "*"
        },
        {
          "Effect": "Allow",
          "Action": [
              "cloudformation:*"
          ],
          "Resource": "*"
      }, 
      {
        "Sid": "DynamoDBIndexAndStreamAccess",
        "Effect": "Allow",
        "Action": [
            "dynamodb:GetShardIterator",
            "dynamodb:Scan",
            "dynamodb:Query",
            "dynamodb:DescribeStream",
            "dynamodb:GetRecords",
            "dynamodb:ListStreams"
        ],
        "Resource": [
            "arn:aws:dynamodb:us-east-1:039168849899:table/Books/index/*",
            "arn:aws:dynamodb:us-east-1:039168849899:table/Books/stream/*"
        ]
    },
    {
        "Sid": "DynamoDBTableAccess",
        "Effect": "Allow",
        "Action": [
            "dynamodb:BatchGetItem",
            "dynamodb:BatchWriteItem",
            "dynamodb:ConditionCheckItem",
            "dynamodb:PutItem",
            "dynamodb:DescribeTable",
            "dynamodb:DeleteItem",
            "dynamodb:GetItem",
            "dynamodb:Scan",
            "dynamodb:Query",
            "dynamodb:UpdateItem"
        ],
        "Resource": "*"
    },
    {
        "Sid": "DynamoDBDescribeLimitsAccess",
        "Effect": "Allow",
        "Action": "dynamodb:DescribeLimits",
        "Resource": [
            "arn:aws:dynamodb:us-east-1:039168849899:table/Books",
            "arn:aws:dynamodb:us-east-1:039168849899:table/Books/index/*"
        ]
    },
    { 
        "Sid": "AttachAnyPolicyToAmazonConnectRole", 
        "Effect": "Allow", 
        "Action": "iam:PutRolePolicy", 
        "Resource": "arn:aws:iam::*:role/aws-service-role/connect.amazonaws.com/AWSServiceRoleForAmazonConnect*" 
    } 

    ]
  }
  
EOF
}



resource "aws_iam_policy_attachment" "cfg_instance-attach" {
  name = "lambda_iam_attachment"
  roles = [aws_iam_role.lambda-function-role.name]
  users      = [aws_iam_user.iam-user.name]
  groups     = [aws_iam_group.iam-group.name]
  #count      = length(var.iam_policy_arn)
  #policy_arn = var.iam_policy_arn[count.index]
  policy_arn = aws_iam_policy.lambda_policies.arn


}

